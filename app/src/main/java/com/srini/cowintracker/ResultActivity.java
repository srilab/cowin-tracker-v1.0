package com.srini.cowintracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.srini.cowintracker.adapters.ResultModel;
import com.srini.cowintracker.adapters.ResultRecyclerAdapter;
import com.srini.cowintracker.databinding.ActivityResultBinding;
import com.srini.cowintracker.model.Center;
import com.srini.cowintracker.model.CentersList;
import com.srini.cowintracker.model.Session;
import com.srini.cowintracker.network.RetrofitApiCall;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResultActivity extends AppCompatActivity {
    private static final String TAG = "ResultActivity";

    private ActivityResultBinding resultBinding;
    private ResultRecyclerAdapter adapter;

    private HttpLoggingInterceptor httpLoggingInterceptor;
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private RetrofitApiCall retrofitApiCall;
    private String BASE_URL="https://cdn-api.co-vin.in/api/";
    
    private ResultModel resultModel;
    private List<ResultModel> resultModelList = new ArrayList<>();

    private String searchKey,searchValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resultBinding = ActivityResultBinding.inflate(getLayoutInflater());
        setContentView(resultBinding.getRoot());

        Bundle bundle = getIntent().getExtras();
        if (bundle.keySet().contains("pincode")){
            searchKey = "pincode";
            searchValue = bundle.getString(searchKey);
        }
        else {
            searchKey = "district";
            Log.d(TAG, "onCreate: "+bundle.getString("dist_name"));
            Log.d(TAG, "onCreate: "+bundle.getString("dist_id"));
            searchValue = bundle.getString("dist_id");
        }
        Log.d(TAG, "onCreate: "+searchKey+searchValue);
//        String valToCheck = getIntent().getExtras().getString("search_val");

        resultBinding.resultTitle.setText(bundle.getString("dist_name"));
        resultBinding.progressCircular.setVisibility(View.VISIBLE);

        initRecylerView();
        initRetrofitCalls();

    }

    private void initRecylerView() {//List<ResultModel> resultModels
        Log.d(TAG, "initRecylerView: ");
        resultBinding.recyclerResult.setHasFixedSize(true);
        resultBinding.recyclerResult.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ResultRecyclerAdapter(resultModelList);
        resultBinding.recyclerResult.setAdapter(adapter);

    }


    private void initRetrofitCalls() {
        Log.d(TAG, "initRetrofitCalls: ");

        String date = "";

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        date =dateFormat.format(new Date());
        Log.d(TAG, "initRetrofitCalls date : "+date);

        httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitApiCall = retrofit.create(RetrofitApiCall.class);

        if (searchKey.equalsIgnoreCase("district")){

            //test line
            //Call<CentersList> dataByDistrictCall = retrofitApiCall.getDataByDistrict(512,"31-03-2021");
            Call<CentersList> dataByDistrictCall = retrofitApiCall.getDataByDistrict(Integer.parseInt(searchValue),date);
            dataByDistrictCall.enqueue(new Callback<CentersList>() {
                @Override
                public void onResponse(Call<CentersList> call, Response<CentersList> response) {
                    if (response.isSuccessful()){
                        Log.d(TAG, "onResponse success...");
                        updateRecyclerData(response.body());
                    }
                    else {
                        Log.d(TAG, "onResponse failure : "+response.code());
                    }
                }

                @Override
                public void onFailure(Call<CentersList> call, Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                }
            });

        }

        else {

            Call<CentersList> dataByPinCall = retrofitApiCall.getDataByPin(Integer.parseInt(searchValue),date);
            dataByPinCall.enqueue(new Callback<CentersList>() {
                @Override
                public void onResponse(Call<CentersList> call, Response<CentersList> response) {
                    if (response.isSuccessful()){
                        Log.d(TAG, "onResponse success...");
                        updateRecyclerData(response.body());
                    }
                    else {
                        Log.d(TAG, "onResponse failure : "+response.code());
                    }

                }

                @Override
                public void onFailure(Call<CentersList> call, Throwable t) {
                    Log.e(TAG, "onFailure: ", t);

                }
            });

        }



    }
    
    private void updateRecyclerData(CentersList body){
        Log.d(TAG, "updateRecyclerData: ");
        resultBinding.progressCircular.setVisibility(View.GONE);

        if (body!=null && body.getCenters().size()!=0){
            resultModelList.clear();
            for (Center mCenter : body.getCenters()) {
                if (mCenter.getSessions().size()!=0){
                    for (Session mSession : mCenter.getSessions()) {
                        resultModel = new ResultModel();
                        resultModel.setName(mCenter.getName());
                        resultModel.setAddress(mCenter.getAddress());
                        resultModel.setFeeType(mCenter.getFeeType());
                        resultModel.setDate(mSession.getDate());
                        resultModel.setAvailableCapacity(mSession.getAvailableCapacity());
                        resultModel.setD1(mSession.getAvailableCapacityDose1());
                        resultModel.setD2(mSession.getAvailableCapacityDose2());
                        resultModel.setVaccine(mSession.getVaccine());
                        resultModel.setMinAge(mSession.getMinAgeLimit());
//                        Log.d(TAG, "updateRecyclerData list->: "+resultModel.getName());
                        resultModelList.add(resultModel);
//                        for (int i = 0; i < resultModelList.size(); i++) {
//                            Log.d(TAG, "updateRecyclerData list->: "+resultModelList.get(i).getName()+" = "+resultModelList.get(i).getDate());
//                        }
                    }
                }

            }
//            for (ResultModel mResult : resultModelList) {
//                Log.d(TAG, "updateRecyclerData: "+mResult.getName());
//            }

            if (resultModelList.size()!=0){
                resultBinding.notFoundText.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
//                initRecylerView(resultModelList);
            }
            else {
                resultBinding.notFoundText.setVisibility(View.VISIBLE);
            }

        }
        else {
            resultBinding.notFoundText.setVisibility(View.VISIBLE);
        }

    }

}