package com.srini.cowintracker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.srini.cowintracker.databinding.ActivitySearchBinding;
import com.srini.cowintracker.model.District;
import com.srini.cowintracker.model.DistrictsList;
import com.srini.cowintracker.model.State;
import com.srini.cowintracker.model.StatesList;
import com.srini.cowintracker.network.RetrofitApiCall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";

    private ActivitySearchBinding searchBinding;
    private ArrayList<String> stateArrayList = new ArrayList<>();
    private ArrayList<String> districtArrayList = new ArrayList<>();
    private ArrayAdapter stateArrayAdapter,districtArrayAdapter;
    private Map<String,Integer> states_map = new HashMap<>();
    private Map<String,Integer> districts_map = new HashMap<>();
    private String selectedState,selectedDistrict;
    //Retrofit
    private HttpLoggingInterceptor httpLoggingInterceptor;
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private RetrofitApiCall retrofitApiCall;
    private String BASE_URL = "https://cdn-api.co-vin.in/api/";

    private String search_value;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchBinding = ActivitySearchBinding.inflate(getLayoutInflater());
        setContentView(searchBinding.getRoot());

        searchBinding.progressHolder.setVisibility(View.VISIBLE);

        initRetrofit();
        initStatePickerField();
        initDistrictPickerField();
        initPincodePickerField();
        populateStateData();
        //Districts are populated once state is selected
        initSearchField();
    }


    private void initRetrofit() {
        Log.d(TAG, "initRetrofit: ");
        httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitApiCall = retrofit.create(RetrofitApiCall.class);
    }

    private void initStatePickerField() {
        Log.d(TAG, "initStatePickerField: ");
        stateArrayAdapter = new ArrayAdapter(this,R.layout.dropdown_item,stateArrayList);
        searchBinding.autoCompleteTextViewState.setAdapter(stateArrayAdapter);
        searchBinding.autoCompleteTextViewState.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedState = adapterView.getItemAtPosition(i).toString();
                Log.d(TAG, "onItemClick selectedState: "+selectedState);
                populateDistrictData(selectedState);
            }
        });

    }

    private void initDistrictPickerField() {
        Log.d(TAG, "initDistrictPickerField: ");
        districtArrayAdapter = new ArrayAdapter(this,R.layout.dropdown_item,districtArrayList);
        searchBinding.autoCompleteTextViewDistrict.setAdapter(districtArrayAdapter);
        searchBinding.autoCompleteTextViewDistrict.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedDistrict = adapterView.getItemAtPosition(i).toString();
                Log.d(TAG, "onItemClick selectedDistrict: "+selectedDistrict);
                searchBinding.searchButton.setText("Search District");
            }
        });

    }

    private void initPincodePickerField(){
        Log.d(TAG, "initPincodePickerField: ");
        searchBinding.pincodeField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length()==0){
                    searchBinding.pincode.setError(null);
                }else if (editable.toString().length()==6){
                    searchBinding.pincode.setError(null);
                    searchBinding.searchButton.setText("Search Pincode");
                }else {
                    searchBinding.pincode.setError("Enter 6-digit pincode");
                }
            }
        });



    }

    private void populateStateData() {
        Log.d(TAG, "getStateData: ");
        Call<StatesList> statesListCall = retrofitApiCall.getStatesList();
        statesListCall.enqueue(new Callback<StatesList>() {
            @Override
            public void onResponse(Call<StatesList> call, Response<StatesList> response) {
                Log.d(TAG, "onResponse: "+response.code());
                //todo
                if (response.isSuccessful()){
                    Log.d(TAG, "onResponse: Successful..");
                    if (response.body().getStatesList().size()>0){//test
                        for (State state: response.body().getStatesList()) {
                            states_map.put(state.getStateName(),state.getStateId());
                        }
                        stateArrayList.clear();
                        stateArrayList.addAll(states_map.keySet());
                        stateArrayAdapter.notifyDataSetChanged();
                        searchBinding.progressHolder.setVisibility(View.INVISIBLE);
                    }

                }else {
                    Log.e(TAG, "onResponse: "+response.code(), null);
                    Toast.makeText(SearchActivity.this, "Api response error..", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<StatesList> call, Throwable t) {
                Log.d(TAG, "onResponse: "+t.getLocalizedMessage());

            }
        });
        
    }

    private void populateDistrictData(String s) {
        Log.d(TAG, "getDistrictData SSS : "+s+" : "+states_map.get(s));
        searchBinding.progressHolder.setVisibility(View.VISIBLE);
        Call<DistrictsList> districtsListCall = retrofitApiCall.getDistrictsList(states_map.get(s));
        districtsListCall.enqueue(new Callback<DistrictsList>() {
            @Override
            public void onResponse(Call<DistrictsList> call, Response<DistrictsList> response) {
                Log.d(TAG, "onResponse: "+response.code());
                //todo
                if (response.isSuccessful()){
                    Log.d(TAG, "onResponse: Successful..");
                    if (response.body().getDistrictsList().size()>0){//test
                        districts_map.clear();
                        for (District district: response.body().getDistrictsList()) {
                            districts_map.put(district.getDistrict_name(),district.getDistrict_id());
                            Log.d(TAG, "onResponse SSS : "+district.getDistrict_name());
                        }
                        districtArrayList.clear();
                        districtArrayList.addAll(districts_map.keySet());
                        Log.d(TAG, "districtArrayList SSS : "+districtArrayList);
                        districtArrayAdapter.notifyDataSetChanged();
                        searchBinding.progressHolder.setVisibility(View.INVISIBLE);
                    }

                }else {
                    Log.e(TAG, "onResponse: "+response.code(), null);
                    Toast.makeText(SearchActivity.this, "Api response error..", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictsList> call, Throwable t) {
                Log.d(TAG, "onResponse SSS DistrictsList: error"+t.getLocalizedMessage());

            }
        });

    }


    private void initSearchField() {
        Log.d(TAG, "initSearchField: ");
        searchBinding.searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: Search -> "+selectedState+" - "+selectedDistrict);
                Log.d(TAG, "onClick: Search -> dist id= "+districts_map.get(selectedDistrict));
                Log.d(TAG, "onClick: Search -> pincode= "+searchBinding.pincodeField.getText().toString());
                //TODO: API - Search

                if (searchBinding.searchButton.getText().toString().toLowerCase().contains("pincode")) {
                    Log.d(TAG, "onClick SSS: pincode");
                    search_value = searchBinding.pincodeField.getText().toString();
                    initResultPage("pincode",search_value);
                }else if (searchBinding.searchButton.getText().toString().toLowerCase().contains("district")){
                    Log.d(TAG, "onClick SSS: district");
//                    search_value = selectedDistrict;
                    initResultPage("district",selectedDistrict);
                }
                else {
                    Toast.makeText(getBaseContext(), "Choose District/Pincode", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


    private void initResultPage(String search_key, String search_value){
        Log.d(TAG, "initResultPage search: "+search_value);
        Bundle bundle = new Bundle();
        if (search_key.equalsIgnoreCase("pincode")){
            bundle.putString(search_key,search_value);
        }
        else {
            bundle.putString("dist_name",selectedDistrict);//selectedDistrict
            bundle.putString("dist_id",districts_map.get(selectedDistrict).toString());
        }
        Intent intent = new Intent(this,ResultActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

}