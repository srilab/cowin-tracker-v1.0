package com.srini.cowintracker.network;

import com.srini.cowintracker.model.CentersList;
import com.srini.cowintracker.model.DistrictsList;
import com.srini.cowintracker.model.StatesList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitApiCall {

    @Headers({"Content-Type: application/json",
            "User-Agent: Android"})
    @GET("v2/admin/location/states")
    Call<StatesList> getStatesList();

    @Headers({"Content-Type: application/json",
            "User-Agent: Android"})
    @GET("v2/admin/location/districts/{id}")
    Call<DistrictsList> getDistrictsList(@Path("id") int state_id);

    @Headers({"Content-Type: application/json",
            "User-Agent: Android"})
    @GET("v2/appointment/sessions/public/calendarByDistrict")
    Call<CentersList> getDataByDistrict(@Query("district_id") int dist_id, @Query("date") String date);


    @Headers({"Content-Type: application/json",
            "User-Agent: Android"})
    @GET("v2/appointment/sessions/public/calendarByPin")
    Call<CentersList> getDataByPin(@Query("pincode") int pincode, @Query("date") String date);




}
