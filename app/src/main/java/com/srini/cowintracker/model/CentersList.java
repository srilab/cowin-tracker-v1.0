package com.srini.cowintracker.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CentersList {
    @SerializedName("centers")
    private List<Center> centers;

    public List<Center> getCenters() {
        return centers;
    }

    public void setCenters(List<Center> centers) {
        this.centers = centers;
    }
}
