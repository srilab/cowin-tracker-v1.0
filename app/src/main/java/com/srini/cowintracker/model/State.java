package com.srini.cowintracker.model;


import com.google.gson.annotations.SerializedName;

public class State {

    @SerializedName("state_id")
    private int stateId;

    @SerializedName("state_name")
    private  String StateName;

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }
}
