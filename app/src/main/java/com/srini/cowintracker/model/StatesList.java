package com.srini.cowintracker.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StatesList {

    @SerializedName("states")
    private List<State> statesList;
//    private int ttl;//present in json

    public List<State> getStatesList() {
        return statesList;
    }

//    public int getTtl() {
//        return ttl;
//    }

    public void setStatesList(List<State> statesList) {
        this.statesList = statesList;
    }

//    public void setTtl(int ttl) {
//        this.ttl = ttl;
//    }

}
