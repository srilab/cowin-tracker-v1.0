package com.srini.cowintracker.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DistrictsList {

    @SerializedName("districts")
    private List<District> districts;

    private int ttl;


    public List<District> getDistrictsList() {
        return districts;
    }

    public void setDistrictsList(List<District> districts) {
        this.districts = districts;
    }
}
