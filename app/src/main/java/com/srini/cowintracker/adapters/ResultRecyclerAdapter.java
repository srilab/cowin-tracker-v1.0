package com.srini.cowintracker.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.srini.cowintracker.databinding.ResultItemLayoutBinding;

import java.util.List;

public class ResultRecyclerAdapter extends RecyclerView.Adapter<ResultRecyclerAdapter.Viewholder>{
    private static final String TAG = "ResultRecyclerAdapter";
    private List<ResultModel> list;

    public ResultRecyclerAdapter(List<ResultModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ResultItemLayoutBinding resultItemLayoutBinding = ResultItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        //LayoutInflater.from(parent.getContext())
        //            .inflate(R.layout.card_listitem, parent, false);
        return new Viewholder(resultItemLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ResultRecyclerAdapter.Viewholder holder, int position) {
        holder.itemLayoutBinding.name.setText(list.get(position).getName());
        holder.itemLayoutBinding.address.setText(list.get(position).getAddress());
        holder.itemLayoutBinding.feeType.setText(list.get(position).getFeeType());
        holder.itemLayoutBinding.date.setText(list.get(position).getDate());
        holder.itemLayoutBinding.availableCapacity.setText("Available Doses- "+String.valueOf(list.get(position).getAvailableCapacity()));
        holder.itemLayoutBinding.d1.setText(" D1 "+String.valueOf(list.get(position).getD1()));
        holder.itemLayoutBinding.d2.setText(" D2 "+String.valueOf(list.get(position).getD2()));
        holder.itemLayoutBinding.vaccine.setText(list.get(position).getVaccine());
        holder.itemLayoutBinding.minAge.setText("Min Age- "+String.valueOf(list.get(position).getMinAge()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        private ResultItemLayoutBinding itemLayoutBinding;
        public Viewholder(@NonNull ResultItemLayoutBinding itemLayoutBinding) {
            super(itemLayoutBinding.getRoot());
            this.itemLayoutBinding =itemLayoutBinding;

        }
    }
}
