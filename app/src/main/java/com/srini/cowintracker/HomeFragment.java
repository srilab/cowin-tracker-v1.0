package com.srini.cowintracker;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.srini.cowintracker.databinding.FragmentHomeBinding;

//import javax.net.ssl.SSLContext;
//import javax.net.ssl.SSLSocketFactory;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.TrustManagerFactory;
//import javax.net.ssl.X509TrustManager;


public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";
    private FragmentHomeBinding homeBinding;
//    private String URL = "https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByCenter?center_id=602825&date=27-06-2021";
    private String BASE_URL = "https://cdn-api.co-vin.in/api/";


    public HomeFragment() { }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeBinding = FragmentHomeBinding.inflate(inflater,container,false);
        return homeBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homeBinding.continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: Continue ");
                Intent intent = new Intent(getActivity(),SearchActivity.class);
                startActivity(intent);
            }
        });

    }





    //test run for cowin api's
    //todo:remove/comment below test run
/*
    private void getApiResponse(boolean is_state) {
        Log.d(TAG, "getApiResponse: SSS");

        // OKHTTP CLIENT IS NEEDED FOR FIRST TIME USING RETROFIT API CALL FROM NEW CLIENT/DEVICE
        // BELOW COMMENTED CODE NOT REQUIRED FOR COWIN PUBLIC API'S , SIMPLE OKKHTTPCLIENT IS ENOUGH

    *//*
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        //TrustManagerFactory
        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
            }
            X509TrustManager trustManager = (X509TrustManager) trustManagers[0];
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[] { trustManager }, null);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            //HTTP Client
            OkHttpClient okHttpClient =new OkHttpClient.Builder()
                    .addInterceptor(httpLoggingInterceptor)
                    .sslSocketFactory(sslSocketFactory, trustManager)
                    .build();
            Log.d(TAG, "test: SSS Http client build .. ");


        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            Log.d(TAG, "test: SSS Http client build error"+e.getMessage());
            e.printStackTrace();
        }
    *//*


        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitApiCall retrofitApiCall = retrofit.create(RetrofitApiCall.class);

        if (is_state){

            Call<StatesList> statesList = retrofitApiCall.getStatesList();

            statesList.enqueue(new Callback<StatesList>() {
                @Override
                public void onResponse(Call<StatesList> statesList, Response<StatesList> response) {
                    Log.d(TAG, "onResponse: SSS "+response.isSuccessful());
                    Log.d(TAG, "onResponse: SSS "+response.code());
                    Log.d(TAG, "onResponse: SSS "+response.body().getStatesList().get(0).getStateName());
//                Log.d(TAG, "onResponse: SSS "+response.body().getTtl());
                *//*for (State states: response.body().getStatesList()) {
                    Log.d(TAG, "onResponse SSS State_id= : "+states.getStateId());
                    Log.d(TAG, "onResponse SSS State_name= : "+states.getStateName());
                }*//*

                }

                @Override
                public void onFailure(Call<StatesList> statesList, Throwable t) {
                    Log.d(TAG, "onResponse: SSS failure"+t.getMessage());

                }
            });

        }else {

            Call<DistrictsList> districtsListCall = retrofitApiCall.getDistrictsList(16);

            districtsListCall.enqueue(new Callback<DistrictsList>() {
                @Override
                public void onResponse(Call<DistrictsList> call, Response<DistrictsList> response) {

                    Log.d(TAG, "onResponse SSS DistrictsList: "+response.body().getDistricts().get(1).getDistrict_name());
                }

                @Override
                public void onFailure(Call<DistrictsList> call, Throwable t) {
                    Log.d(TAG, "onResponse SSS DistrictsList: error"+t.getLocalizedMessage());

                }
            });

        }


    }

    */

}